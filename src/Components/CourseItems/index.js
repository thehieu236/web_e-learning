import React, { Component } from "react";

class CourseItem extends Component {
  render() {
    let { tenKhoaHoc, hinhAnh, nguoiTao } = this.props.item;
    // maKhoaHoc(pin):"123"
    // biDanh(pin):"node-js"
    // tenKhoaHoc(pin):"Node js"
    // moTa(pin):"mo ta tu duy lap trinh"
    // luotXem(pin):100
    // hinhAnh(pin):"https://elearning0706.cybersoft.edu.vn/hinhanh/node-js_gp01.png"
    // maNhom(pin):"gp01"
    // ngayTao(pin):"05/12/2022"
    // soLuongHocVien(pin):0
    return (
      <div className="card mt-3 px-2">
        <div className="card-body" style={{ height: "450px" }}>
          <img
            src={hinhAnh}
            alt=""
            style={{ width: "100%", height: "200px" }}
          />
          <p className="lead">{tenKhoaHoc}</p>
          <p className="lead">Instructor: {nguoiTao.hoTen}</p>
          <p className="lead">Rating: 5.0</p>
        </div>
        <button className="btn btn-success mb-2">Go To Detail</button>
      </div>
    );
  }
}

export default CourseItem;
