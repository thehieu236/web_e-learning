import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { userService } from "../../Services";
import { signupUserSchema } from "../../Services/user";

class SignupScreen extends Component {
  handleSubmit = (values) => {
    userService
      .signUp(values)
      .then((res) => {
        console.log("res", res);
      })
      .catch((err) => {
        console.log("err", err);
      });
  };

  render() {
    return (
      <div className="w-50 mx-auto">
        <h1 className="display-4 text-center">Sign up</h1>

        <Formik
          initialValues={{
            taiKhoan: "",
            matKhau: "",
            hoTen: "",
            email: "",
            soDT: "",
            maNhom: "GP01",
          }}
          validationSchema={signupUserSchema}
          onSubmit={this.handleSubmit}
          render={(formikProps) => (
            <Form>
              <div className="form-group">
                <label>Tài khoản: </label>
                <Field
                  name="taiKhoan"
                  onChange={formikProps.handleChange}
                  type="text"
                  className="form-control"
                />
                <ErrorMessage name="taiKhoan" />
              </div>
              <div className="form-group ">
                <label>Mật khẩu: </label>
                <Field
                  name="matKhau"
                  onChange={formikProps.handleChange}
                  type="password"
                  className="form-control"
                />
                <ErrorMessage name="matKhau" />
              </div>
              <div className="form-group">
                <label>Họ tên: </label>
                <Field
                  name="hoTen"
                  onChange={formikProps.handleChange}
                  type="text"
                  className="form-control"
                />
                <ErrorMessage name="hoTen" />
              </div>
              <div className="form-group">
                <label>Email: </label>
                <Field
                  name="email"
                  onChange={formikProps.handleChange}
                  type="email"
                  className="form-control"
                />
                <ErrorMessage name="email" />
              </div>
              <div className="form-group">
                <label>Số điện thoại: </label>
                <Field
                  name="soDT"
                  onChange={formikProps.handleChange}
                  type="text"
                  className="form-control"
                />
                <ErrorMessage name="soDT" />
              </div>
              <div className="form-group">
                <label>Mã nhóm: </label>
                <Field
                  component="select"
                  name="maNhom"
                  onChange={formikProps.handleChange}
                  className="form-control"
                >
                  <option value="GP01">GP01</option>
                  <option value="GP02">GP02</option>
                  <option value="GP03">GP03</option>
                  <option value="GP04">GP04</option>
                  <option value="GP05">GP05</option>
                  <option value="GP06">GP06</option>
                  <option value="GP07">GP07</option>
                  <option value="GP08">GP08</option>
                  <option value="GP09">GP09</option>
                  <option value="GP10">GP10</option>
                </Field>
                <ErrorMessage name="namNhom" />
              </div>
              <div className="text-center">
                <button type="submit" className="btn btn-success">
                  Submit
                </button>
              </div>
            </Form>
          )}
        />
      </div>
    );
  }
}

export default SignupScreen;
