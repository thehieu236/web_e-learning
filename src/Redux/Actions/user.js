import { courseService } from "../../Services";
import { createAction } from "./index";
import { FETCH_COUSRES } from "./type";

export const fetchCourses = () => {
  return (dispatch) => {
    courseService
      .fetchCourse()
      .then((res) => {
        dispatch(createAction(FETCH_COUSRES, res.data));
      })
      .catch((err) => {
        console.log("err", err);
      });
  };
};
