import Axios from "axios";

class CourseService {
  fetchCourse() {
    return Axios({
      method: "GET",
      url: "https://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=123",
    });
  }

  fetchCourseDetail() {
    return Axios({
      method: "GET",
      url: "https://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=123",
    });
  }
}

export default CourseService;
